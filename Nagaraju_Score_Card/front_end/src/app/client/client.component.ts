import { Component, OnInit } from "@angular/core";
import { CommonService } from "../common.service";

@Component({
  selector: "app-client",
  templateUrl: "./client.component.html",
  styleUrls: ["./client.component.css"]
})
export class ClientComponent implements OnInit {
  scoreCard: Array<any> = [
    {
      ball1: "",
      ball2: "",
      ball3: "",
      ball4: "",
      ball5: "",
      ball6: ""
    }
  ];
  i: any = 0;
  isImg: boolean = false;

  constructor(private commonService: CommonService) {}

  ngOnInit() {
    this.commonService.getScore().subscribe(msg => {
      let response = this.scoreCard[this.scoreCard.length - 1];
      for (let obj in response) {
        if (response[obj] == "") {
          response[obj] = msg;
          this.scoreCard[this.scoreCard.length - 1] = response;
          this.i++;
          break;
        }
      }
      if (this.i == 0) {
        this.scoreCard.push({
          ball1: msg,
          ball2: "",
          ball3: "",
          ball4: "",
          ball5: "",
          ball6: ""
        });
      }
      if (msg == 6) {
        this.playAudio();
        setTimeout(() => {
          this.isImg = false;
        }, 4000);
        this.isImg = true;
      }
      this.i = 0;
    });
  }

  playAudio() {
    let audio = new Audio();
    audio.src = "../../../assets/audio.mp3";
    audio.load();
    audio.play();
  }
}
