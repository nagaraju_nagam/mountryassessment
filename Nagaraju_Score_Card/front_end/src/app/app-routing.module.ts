import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonComponent } from "./common/common.component";
import { AdminComponent } from "./admin/admin.component";
import { ClientComponent } from "./client/client.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    component: CommonComponent
  },
  {
    path: "admin",
    component: AdminComponent
  },
  {
    path: "client",
    component: ClientComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
