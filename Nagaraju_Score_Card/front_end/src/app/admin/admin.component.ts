import { Component, OnInit } from "@angular/core";
import { CommonService } from "../common.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
})
export class AdminComponent implements OnInit {
  val: any;
  constructor(private commonService: CommonService) {}

  ngOnInit() {}

  getVal(event: any) {
    this.val = event.target.value;
  }

  sendScore() {
    this.commonService.sendScore(this.val);
  }
}
