import { Injectable } from "@angular/core";
import * as io from "socket.io-client";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  url = "http://localhost:8080";
  socket;

  constructor() {
    this.socket = io(this.url);
  }

  sendScore(msg) {
    this.socket.emit("score", msg);
  }

  getScore(): Observable<any> {
    return Observable.create(observer => {
      this.socket.on("getscore", msg => {
        observer.next(msg);
      });
    });
  }


}
