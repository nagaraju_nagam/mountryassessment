let express = require("express"),
  app = express();

app.get("/", (req, res) => {
  res.send("hello world");
});

const server = app.listen(8080, (req, res) => {
  console.log("-- server started at 8080");
});

const io = require("socket.io")(server);

io.on("connection", socket => {
  console.log("-- socket connected successfully --", socket.id);

  socket.on("score", msg => {
    console.log("--- message ---", msg);
    io.emit("getscore", msg);
  });

});
